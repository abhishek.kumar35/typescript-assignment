"use strict";
class Person {
    constructor(name, age, salary, sex) {
        (this.name = name),
            (this.age = age),
            (this.salary = salary),
            (this.sex = sex);
    }
    static sort(arr, field, order) {
        if (arr.length <= 1) {
            return arr;
        }
        const pivot = arr[arr.length - 1][field];
        const left = [];
        const right = [];
        for (const element of arr.slice(0, arr.length - 1)) {
            if (order === "asc") {
                if (element[field] < pivot) {
                    left.push(element);
                }
                else
                    right.push(element);
            }
            else if (order === "dsc") {
                if (element[field] > pivot) {
                    left.push(element);
                }
                else
                    right.push(element);
            }
        }
        const sortedarray = [...this.sort(left, field, order), arr[arr.length - 1], ...this.sort(right, field, order),
        ];
        return sortedarray;
    }
}
const person1 = new Person("Abhi", 22, 15000, "male");
const person2 = new Person("Riya", 24, 45000, "female");
const person3 = new Person("Putin", 52, 75000, "male");
const person4 = new Person("Watt", 33, 45000, "male");
const person5 = new Person("Scarlett", 32, 65000, "female");
const person6 = new Person("Tom", 32, 36000, "male");
const person7 = new Person("Laura", 21, 15000, "female");
const person8 = new Person("Penny", 32, 10000, "fmale");
const array = [
    person1,
    person2,
    person3,
    person4,
    person5,
    person6,
    person7,
    person8,
];

//sorting by name field in ascending order
console.log(Person.sort(array, "name", "asc"));
//sorting by age field in ascending order
console.log(Person.sort(array, "age", "asc"));
//sorting by salary field in ascending order
console.log(Person.sort(array, "salary", "asc"));
//sorting by name field in descending order
console.log(Person.sort(array, "name", "dsc"));
//sorting by age field in descending order
console.log(Person.sort(array, "age", "dsc"));
//sorting by salary field in descending order
console.log(Person.sort(array, "salary", "dsc"));
